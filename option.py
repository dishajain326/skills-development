import tkinter as tk
import sqlite3
from tkinter import messagebox

# Function to display skills and corresponding content
def display_skills():
    # Function to create SQLite database and tables
    def create_database():
        conn = sqlite3.connect('skills_database.db')
        cursor = conn.cursor()

        # Create a table to store skills information
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS skills (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                skill_name TEXT NOT NULL,
                image_url TEXT,
                video_url TEXT,
                website_url TEXT
            )
        ''')

        conn.commit()
        conn.close()

    # Function to insert skill into the database if it doesn't exist
    def insert_skill_into_database(skill_name, image_url, video_url, website_url):
        conn = sqlite3.connect('skills_database.db')
        cursor = conn.cursor()

        # Check if the skill already exists
        cursor.execute('SELECT * FROM skills WHERE skill_name = ?', (skill_name,))
        existing_skill = cursor.fetchone()

        if not existing_skill:
            # Insert the skill if it doesn't exist
            cursor.execute('''
                INSERT INTO skills (skill_name, image_url, video_url, website_url)
                VALUES (?, ?, ?, ?)
            ''', (skill_name, image_url, video_url, website_url))

        conn.commit()
        conn.close()

    # Create a new window for displaying skills
    skills_window = tk.Toplevel()
    skills_window.title("Skills Required for Engineering Students")
    skills_window.configure(bg="#ADD8E6")  # Set background color

    # Create frames for display content and suggested skills
    content_frame = tk.Frame(skills_window, bg="#ADD8E6")
    content_frame.pack(side=tk.TOP, padx=10, pady=10, fill=tk.BOTH, expand=True)

    suggested_frame = tk.Frame(skills_window, bg="#ADD8E6")
    suggested_frame.pack(side=tk.BOTTOM, padx=10, pady=(0, 20), fill=tk.BOTH, expand=True)

    # Create SQLite database and tables
    create_database()

    # Dictionary containing skills and corresponding content URLs
    skills_content = {
        ' Java': {
            'image': 'C:/Users/pavan/OneDrive/Wise/java.png',
            'video': 'https://www.youtube.com/watch?v=rfscVS0vtbw',
            'website': 'https://www.javatpoint.com/java-tutorial'
        },
        ' Python': {
            'image': 'C:/Users/pavan/OneDrive/Wise/python.png',
            'video': 'https://www.youtube.com/watch?v=rfscVS0vtbw',
            'website': 'https://www.javatpoint.com/python-tutorial'
        },
        ' Communication Skills': {
            'image': 'C:/Users/pavan/OneDrive/Wise/cskills.png',
            'video': 'https://youtu.be/u16EPwFmdis?si=l79KbYtoasi2Q0ye',
            'website': 'https://www.tutorialspoint.com/management_concepts/effective_communication_skills.htm'
        },
        ' Java Script':{
            'image':'C:/Users/pavan/OneDrive/Wise/javascript.png',
            'video':'https://youtu.be/W6NZfCO5SIk?si=Ew6F8JHuSdy_7rta',
            'website':'https://www.javatpoint.com/javascript-tutorial'
        },
        ' CSS':{
            'image':'C:/Users/pavan/OneDrive/Wise/css.png',
            'video':'https://youtu.be/ESnrn1kAD4E?si=imab_T6-XOIK9BCj',
            'website':'https://www.javatpoint.com/css-tutorial'
        },
        ' SQL':{
            'image':'C:/Users/pavan/OneDrive/Wise/sql.png',
            'video':'https://youtu.be/zbMHLJ0dY4w?si=dBpUlJoD4l55kXdE',
            'website':'https://www.javatpoint.com/sql-tutorial'
        },
        ' HTML':{
            'image':'C:/Users/pavan/OneDrive/Wise/html.png',
            'video':'https://youtu.be/qz0aGYrrlhU?si=NVoPHa4nH69TXdUF',
            'website':'https://www.javatpoint.com/html-tutorial'
        },
        ' Engineering - Mathematics':{
            'image':'C:/Users/pavan/OneDrive/Wise/maths1.png',
            'video':'https://youtu.be/UWOVipHUZaM?si=xPsgjmMIgOjq1z1d',
            'website':'https://www.geeksforgeeks.org/engineering-mathematics-tutorials'
        },
        ' Programming':{
            'image':'C:/Users/pavan/OneDrive/Wise/pro.png',
            'video':'https://www.youtube.com/watch?v=ifo76VyrBYo',
            'website':'https://www.javatpoint.com/what-is-computer-programming'
        },
        ' Physics':{
            'image':'C:/Users/pavan/OneDrive/Wise/ap1.png',
            'video':'https://www.youtube.com/watch?v=5mGmZyRyiG8',
            'website':'https://www.javatpoint.com/physics'
        },
        ' Chemistry':{
             'image':'C:/Users/pavan/OneDrive/Wise/chem.png',
            'video':'https://www.youtube.com/watch?v=9P32dGXKGw4&list=PLLQAWQRZZ8UnuKwiiFPAqZFyiTqwEv4-5',
            'website':'https://www.javatpoint.com/analytical-chemistry-definition'
        },
        ' Mechanical Engineering':{
            'image':'C:/Users/pavan/OneDrive/Wise/mech.png',
            'video':'https://www.youtube.com/watch?v=NDa3AGPobS4',
            'website':'https://www.javatpoint.com/engineering-mechanics'
        },
        ' Electrical Engineering':{
             'image':'C:/Users/pavan/OneDrive/Wise/EEE.png',
            'video':'https://www.youtube.com/watch?v=ngo3ZTrT69A',
            'website':'https://www.twi-global.com/technical-knowledge/faqs/what-is-electrical-engineering#:~:text=One%20of%20the%20more%20recent,use%20electricity%2C%20electronics%20and%20electromagnetism.'
        },
        ' Civil Engineering':{
             'image':'C:/Users/pavan/OneDrive/Wise/civil.png',
            'video':'https://www.youtube.com/watch?v=bFljMHTQ1QY',
            'website':'https://www.britannica.com/technology/civil-engineering'
        },
        ' Computer Science':{
             'image':'C:/Users/pavan/OneDrive/Wise/comp.png',
            'video':'https://www.youtube.com/watch?v=CNFK86hJRfE',
            'website':'https://www.javatpoint.com/what-is-computer-science'
        },
        ' Data Structures & Algorithms':{
             'image':'C:/Users/pavan/OneDrive/Wise/DS.png',
            'video':'https://www.youtube.com/watch?v=NDa3AGPobS4',
            'website':'https://www.javatpoint.com/data-structure-introduction'
        },
        ' Digital Logic Design':{
             'image':'C:/Users/pavan/OneDrive/Wise/DLD.png',
            'video':'https://www.youtube.com/watch?v=O0gtKDu_cJc&list=PLxCzCOWd7aiGmXg4NoX6R31AsC5LeCPHe',
            'website':'https://www.geeksforgeeks.org/digital-electronics-logic-design-tutorials/'
        },
        ' Control Systems':{
             'image':'C:/Users/pavan/OneDrive/Wise/cs.png',
            'video':'https://www.youtube.com/watch?v=NDa3AGPobS4',
            'website':'https://www.javatpoint.com/control-system-tutorial'
        },
        ' Communication Systems':{
             'image':'C:/Users/pavan/OneDrive/Wise/com sys.png',
            'video':'https://www.google.com/search?q=communication+systems&source=lmns&tbm=vid&bih=695&biw=1536&hl=en&sa=X&ved=2ahUKEwik8MuGmcuEAxUha2wGHccWCOsQ0pQJKAN6BAgBEAg#fpstate=ive&vld=cid:4caa4835,vid:dWa1zjUVMtY,st:0',
            'website':'https://www.javatpoint.com/digital-communication'
        },
        ' Embedded Systems':{
             'image':'C:/Users/pavan/OneDrive/Wise/embedded.png',
            'video':'https://www.youtube.com/watch?v=nccWuB5ypxI&list=PLcbIZiT62e1gNZ-VWPO3rpTpXkHBMZa2n',
            'website':'https://www.javatpoint.com/embedded-system-tutorial'
        },
        ' Robotics':{
             'image':'C:/Users/pavan/OneDrive/Wise/Robotics.png',
            'video':'https://www.google.com/search?q=robotics+&sca_esv=85af15397c77c0f6&biw=1536&bih=695&tbm=vid&ei=NavdZfrdOdvn2roPsNypoAQ&ved=0ahUKEwi6zN-tmsuEAxXbs1YBHTBuCkQQ4dUDCA0&uact=5&oq=robotics+&gs_lp=Eg1nd3Mtd2l6LXZpZGVvIglyb2JvdGljcyAyChAAGIAEGIoFGEMyCBAAGIAEGLEDMgsQABiABBixAxiDATILEAAYgAQYsQMYgwEyCxAAGIAEGLEDGIMBMggQABiABBixAzIFEAAYgAQyBRAAGIAEMgsQABiABBixAxiDATIFEAAYgARItgxQjwFYhwdwAHgAkAEBmAH3AaAByAmqAQUwLjMuM7gBA8gBAPgBAZgCAqACpQPCAgYQABgWGB7CAgsQABiABBiKBRiGA5gDAIgGAZIHAzItMg&sclient=gws-wiz-video#fpstate=ive&vld=cid:b71ad589,vid:rq_Xl71d3tE,st:0',
            'website':'https://www.javatpoint.com/what-is-robotics'
        },
        ' Machine Learning':{
             'image':'C:/Users/pavan/OneDrive/Wise/https://www.javatpoint.com/dbms-tutorialML.png',
            'video':'https://www.youtube.com/watch?v=ukzFI9rgwfU&list=PLEiEAq2VkUULYYgj13YHUWmRePqiu8Ddy',
            'website':'https://www.javatpoint.com/machine-learning'
        },
        ' Artificial Intelligence':{
             'image':'C:/Users/pavan/OneDrive/Wise/AI.png',
            'video':'https://www.youtube.com/watch?v=JMUxmLyrhSk',
            'website':'https://www.javatpoint.com/artificial-intelligence-ai'
        },
        ' Software Development':{
             'image':'C:/Users/pavan/OneDrive/Wise/software dev.png',
            'video':'view-source:https://www.youtube.com/watch?v=pquPUX1EihM',
            'website':'https://www.javatpoint.com/software-development-life-cycle'
        },
        ' Web Development':{
             'image':'C:/Users/pavan/OneDrive/Wise/WAD.png',
            'video':'https://www.youtube.com/watch?v=gQojMIhELvM&list=PLoYCgNOIyGAB_8_iq1cL8MVeun7cB6eNc',
            'website':'https://www.geeksforgeeks.org/web-development'
        },
        ' DataBase Management':{
             'image':'C:/Users/pavan/OneDrive/Wise/dbms.png',
            'video':'https://www.google.com/search?q=database+management+javatpoint&source=lmns&tbm=vid&bih=695&biw=1536&hl=en&sa=X&ved=2ahUKEwjL5bmCncuEAxV_cWwGHXbDBlIQ0pQJKAN6BAgBEAg#fpstate=ive&vld=cid:2f170ee9,vid:mqprM5YUdpk,st:0',
            'website':'https://www.javatpoint.com/dbms-tutorial'
        },
        ' Network Security':{
             'image':'C:/Users/pavan/OneDrive/Wise/Network-Security-Model.png',
            'video':'https://www.google.com/search?sca_esv=85af15397c77c0f6&hl=en&q=network+security+javatpoint&tbm=vid&source=lnms&sa=X&ved=2ahUKEwi20NjwncuEAxUMnmMGHUzWCT0Q0pQJegQIChAB&biw=1536&bih=695&dpr=1.25#fpstate=ive&vld=cid:b7e5c80d,vid:NQ1cvwEvh44,st:0',
            'website':'https://www.javatpoint.com/what-is-network-security'
        },
        ' Project Management':{
             'image':'C:/Users/pavan/OneDrive/Wise/PM.png',
            'video':'https://www.google.com/search?q=project+management+&sca_esv=85af15397c77c0f6&hl=en&biw=1536&bih=695&tbm=vid&ei=07DdZd3NEMSu0-kP0PqAsAU&ved=0ahUKEwidg4_bn8uEAxVE1zQHHVA9AFYQ4dUDCA0&uact=5&oq=project+management+&gs_lp=Eg1nd3Mtd2l6LXZpZGVvIhNwcm9qZWN0IG1hbmFnZW1lbnQgMgoQABiABBiKBRhDMgsQABiABBixAxiDATILEAAYgAQYsQMYgwEyBRAAGIAEMgsQABiABBixAxiDATILEAAYgAQYsQMYgwEyBRAAGIAEMgUQABiABDIFEAAYgAQyBRAAGIAESKIOUNMBWKIKcAB4AJABAZgB7QGgAakLqgEFMC40LjO4AQPIAQD4AQGYAgSgArUGwgIGEAAYFhgewgILEAAYgAQYigUYhgOYAwCIBgGSBwUwLjMuMQ&sclient=gws-wiz-video#fpstate=ive&vld=cid:93c649db,vid:pc9nvBsXsuM,st:0',
            'website':'https://www.javatpoint.com/what-is-project-management'
        },
        ' Problem Solving':{
             'image':'C:/Users/pavan/OneDrive/Wise/ps.png',
            'video':'https://www.youtube.com/watch?v=cybMiQYuEwc&list=PLH1n1sJO7tbxmE36txTPhgidmW5Z9Bn7m',
            'website':'https://www.javatpoint.com/aptitude/quantitative'
        },
        ' Critical Thinking':{
             'image':'C:/Users/pavan/OneDrive/Wise/CT.png',
            'video':'https://www.google.com/search?q=critical+thinking&source=lmns&tbm=vid&bih=695&biw=1536&hl=en&sa=X&ved=2ahUKEwiUnbT5oMuEAxUiZmwGHfinD04Q0pQJKAR6BAgBEAo#fpstate=ive&vld=cid:e8875c35,vid:6OLPL5p0fMg,st:0',
            'website':'https://www.tutorialspoint.com/critical_thinking/index.htm'
        },
        ' Team Work':{
             'image':'C:/Users/pavan/OneDrive/Wise/Benefits-Of-Teamwork-min.png',
            'video':'https://youtu.be/t6uAM_66pK0?feature=shared',
            'website':'https://www.javatpoint.com/group-discussion'
        },
        ' C':{
             'image':'C:/Users/pavan/OneDrive/Wise/c.png',
            'video':'https://www.youtube.com/watch?v=f0RHzB8lC98',
            'website':'https://www.javatpoint.com/c-programming-language-tutorial'
        },
        ' C++':{
             'image':'C:/Users/pavan/OneDrive/Wise/c++.png',
            'video':'https://youtu.be/ZzaPdXTrSb8?feature=shared',
            'website':'https://www.javatpoint.com/cpp-tutorial'
        },
        # ... (rest of the skills)
    }

    # Function to display content for the selected skill
    def display_content():
        # Get the selected skill
        selected_skill = skill_listbox.get(skill_listbox.curselection())

        # Retrieve skill details from the database
        conn = sqlite3.connect('skills_database.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM skills WHERE skill_name = ?', (selected_skill,))
        skill_details = cursor.fetchone()
        conn.close()

        if skill_details:
            # Create a new window for displaying content
            content_window = tk.Toplevel()
            content_window.title(selected_skill)
            content_window.configure(bg="#ADD8E6")  # Set background color

            # Display image for the selected skill
            image_url = skill_details[2]
            image_label = tk.Label(content_window)
            image_label.pack(padx=10, pady=10)
            image_label.img = tk.PhotoImage(file=image_url)
            image_label.config(image=image_label.img)

            # Display video for the selected skill
            video_url = skill_details[3]
            video_label = tk.Label(content_window, text=f"Reference Video: {video_url}", font=("Arial", 20), bg="#ADD8E6")
            video_label.pack(padx=10, pady=10)

            # Add a hyperlink to the video URL
            def open_video():
                import webbrowser
                webbrowser.open(video_url)
            video_label.bind("<Button-1>", lambda e: open_video())

            # Display website link for the selected skill
            website_url = skill_details[4]
            website_label = tk.Label(content_window, text=f"Reference Website: {website_url}", font=("Arial", 20), bg="#ADD8E6", fg="blue", cursor="hand2")
            website_label.pack(padx=10, pady=10)
            website_label.bind("<Button-1>", lambda e: open_website(website_url))

    # Function to open website link
    def open_website(website_url):
        import webbrowser
        webbrowser.open(website_url)

    # Create a Listbox to display skills
    skill_listbox = tk.Listbox(content_frame, width=60, height=10, font=("Arial", 18), borderwidth=2, highlightbackground="black", bg="white")
    skill_listbox.pack(padx=5)

    # Add skills to the Listbox and insert them into the database
    for skill, content in skills_content.items():
        skill_listbox.insert(tk.END, skill)
        insert_skill_into_database(skill, content['image'], content['video'], content['website'])

    # Button to display content for the selected skill
    display_button = tk.Button(content_frame, fg='white', bg='black', text="Display Content", font=("Arial", 18), borderwidth=5, highlightbackground="black", command=display_content)
    display_button.pack(pady=30)

    # Entry for suggesting a new skill
    label_text = tk.Label(skills_window, text="Want to add any Skills? Please enter Below", font=("Arial", 16), bg="lightblue")
    label_text.pack(padx=10, pady=5)
    entry = tk.Entry(suggested_frame, width=40, font=("Arial", 20), borderwidth=3, highlightbackground="black", fg='black')
    entry.pack(padx=10, pady=5)

    # Create a Listbox to display suggested skills
    suggested_listbox = tk.Listbox(suggested_frame, width=50, height=5, font=("Arial", 18), borderwidth=3, highlightbackground="black")
    suggested_listbox.pack(padx=10, pady=10)

    # Function to add suggested skill to the listbox and insert into the database
    def suggest_skill():
        new_skill = entry.get()
        if new_skill:
            suggested_listbox.insert(tk.END, new_skill)
            entry.delete(0, tk.END)
            suggest_message.config(text=f"Thanks for suggesting the skill: {new_skill}")
            insert_skill_into_database(new_skill, '', '', '')

    # Button to suggest a new skill
    suggest_button = tk.Button(suggested_frame, fg='white', bg='black', text="Suggest Skill", font=("Arial", 18), borderwidth=5, highlightbackground="black", command=suggest_skill)
    suggest_button.pack(pady=5)
    suggest_message = tk.Label(suggested_frame, text="", font=("Arial", 18), bg="#ADD8E6")
    suggest_message.pack(pady=5)

# Create the main Tkinter window
root = tk.Tk()
root.title("Skills Development - Skills Required")
root.geometry("400x200")
root.configure(bg="lightblue")  # Set background color

# Create a button to display skills
button = tk.Button(root, bg='white', fg='black', text="Display Skills", font=("Arial", 20), command=display_skills, borderwidth=7, highlightbackground="black")
button.pack(pady=10, anchor="center")

title_label = tk.Label(root, text="To display the skills, please select the above button", font=("Helvetica", 20), bg="#ADD8E6")
title_label.pack(side="top", anchor="center", pady=10)

# Run the Tkinter main loop
root.mainloop()