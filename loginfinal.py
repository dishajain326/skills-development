# login.py
import customtkinter
from tkinter import *
from tkinter import messagebox
from db import login

def open_next_page(username):
    app.destroy()
    import option

app = Tk()
app.title('Login')
app.geometry("800x300")
app.config(bg='lightblue')

font1 = ('Helvetica', 16, 'bold')

# Adjust the width and height of the frame
frame_width = 280
frame_height = 400

# Calculate the x and y positions to align the frame in the center
x_pos = (app.winfo_screenwidth() // 2) - (frame_width // 2)
y_pos = (app.winfo_screenheight() // 2) - (frame_height // 2)

frame3 = customtkinter.CTkFrame(app, bg_color='lightblue', fg_color='white', width=frame_width, height=frame_height)
frame3.place(x=x_pos, y=y_pos)

def login_action():
    username = username_entry.get()
    password = password_entry.get()

    if username != '' and password != '':
        if login(username, password):
            messagebox.showinfo('Success', 'Login successful.')
            open_next_page(username)
        else:
            messagebox.showerror('Error', 'Username or password is invalid!!!')
    else:
        messagebox.showerror('Error', 'Enter all data.')
username_label = Label(frame3, font=font1, text='Username:', bg='lightblue')
username_label.pack(pady=7)
username_entry = Entry(frame3,width=20,font=font1,borderwidth=3)
username_entry.pack(pady=10)

password_label = Label(frame3, font=font1, text='Password:', bg='lightblue')
password_label.pack(pady=7)
password_entry = Entry(frame3,width=20,borderwidth=3,font=font1, show='*')
password_entry.pack(pady=10)

login_button = Button(frame3, text='Login', command=login_action, width=10,font=font1, bg='green', fg='white')
login_button.pack(pady=20)

# "Don't have an account" link
signup_link = Label(frame3, text="Don't have an account? Click here to sign up", font=('Arial', 11), fg='blue', cursor='hand2')
signup_link.pack(pady=10)

# Function to open signup window
def open_signup_window():
    app.destroy()  # Close the login window
    import signupfinal  # Import and run the signup script

signup_link.bind("<Button-1>", lambda e: open_signup_window())

app.mainloop()
