# signup.py
import customtkinter
from tkinter import *
from tkinter import messagebox
from db import create_table, signup

def open_next_page(username):
    app.destroy()
    import loginfinal

app = Tk()
app.title('Sign Up')
app.geometry("800x300")
app.config(bg='lightblue')

font1 = ('Helvetica', 16, 'bold')
frame_width = 280
frame_height = 400

x_pos = (app.winfo_screenwidth() // 2) - (frame_width // 2)
y_pos = (app.winfo_screenheight() // 2) - (frame_height // 2)


frame3 = customtkinter.CTkFrame(app, bg_color='lightblue', fg_color='white', width=frame_width, height=frame_height)
frame3.place(x=x_pos, y=y_pos)
def signup_action():
    username = username_entry.get()
    password = password_entry.get()

    if username != '' and password != '':
        if signup(username, password):
            messagebox.showinfo('Success', 'Welcome,'+username)
            open_next_page(username)
        else:
            messagebox.showerror('Error', 'Username already exists.')
    else:
        messagebox.showerror('Error', 'Enter all data.')

username_label = Label(frame3, font=font1, text='Username:', bg='white')
username_label.pack(pady=7)
username_entry = Entry(frame3,width=20,font=font1,borderwidth=3)
username_entry.pack(pady=10)

password_label = Label(frame3, font=font1, text='Password:', bg='white')
password_label.pack(pady=7)
password_entry = Entry(frame3,width=20,borderwidth=3,font=font1, show='*')
password_entry.pack(pady=10)

signup_button = Button(frame3, text='Sign Up', command=signup_action, font=font1, bg='green', fg='white')
signup_button.pack(pady=20)

# "Already have an account" link
login_link = Label(frame3, text="Already have an account? Click here to login", font=('Arial', 10), fg='blue', cursor='hand2')
login_link.pack(pady=10)

# Function to open login window
def open_login_window():
    app.destroy()  # Close the signup window
    import loginfinal  # Import and run the login script

login_link.bind("<Button-1>", lambda e: open_login_window())

app.mainloop()