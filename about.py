import tkinter as tk

def open_about_page():
    # Create a new window for the about page
    about_window = tk.Tk()
    about_window.title("About Skills Development")
    about_window.configure(bg="#ADD8E6")

    # Add content to the about page
    label = tk.Label(about_window, text="About Skills Development", font=("Arial", 22, "bold"), bg="#ADD8E6")
    label.pack(pady=10)

    about_text = """
    Skills Development is a project aimed at providing resources and support for engineering students to enhance their 
    Skills.This application provides videos, images, and text regarding various 
    skills that engineering students need to develop.
    """
    about_label = tk.Label(about_window, text=about_text,font=("Arial", 22), bg="#ADD8E6")
    about_label.pack(pady=5)

    about_window.mainloop()
# Create the main Tkinter window
# root = tk.Tk()

# Call the function to open the about page immediately upon running the script
open_about_page()

# Run the Tkinter event loop
# root.mainloop()
