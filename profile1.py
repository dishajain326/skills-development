import tkinter as tk

def open_profile_page():
    # Create a new window for the profile page
    profile_window = tk.Tk()
    profile_window.title("Profile Page\n")
    profile_window.configure(bg="#ADD8E6")

    # Add content to the profile page
    label = tk.Label(profile_window, text="Profile Page", font=("Arial", 20, "bold"))
    label.pack(pady=10)

    # Example profile information
    profile_info = {
        "Name": "John Doe",
        "Age": 30,
        "Email": "john.doe@example.com",
        "Address": "123 Main Street, City, Country"
    }

    # Display profile information
    for key, value in profile_info.items():
        info_label = tk.Label(profile_window, font=("Arial", 22),text=f"{key}: {value}")
        info_label.pack(pady=5)

    profile_window.mainloop()
# Create the main Tkinter window
# root = tk.Tk()
# root.title("Profile Page")

# Call the function to open the profile page immediately upon running the script
open_profile_page()

# Run the Tkinter event loop
# root.mainloop()
