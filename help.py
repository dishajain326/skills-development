import tkinter as tk

def open_help_page():
    # Create a new window for the help page
    help_window = tk.Tk()
    help_window.title("Help - Skills Development")
    help_window.configure(bg="#ADD8E6")

    # Add content to the help page
    label = tk.Label(help_window, text="Help Page - Skills Development", font=("Arial", 20, "bold"), bg="#ADD8E6")
    label.pack(pady=10)

    help_text = """
    Welcome to the Skills Development project!

    This application is designed to provide resources and support for engineering students to enhance their skills.
    Here are some key features of the application:

    - Home: Clicking on the "Home" button will take you to the main page of the application.
    - Profile: The "Profile" button allows you to view and update your profile information.
    - About: The "About" page provides information about the Skills Development project.
    - Help: You are currently viewing the Help page, which provides guidance on using the application.

    For further assistance, please contact support@example.com.
    """
    help_label = tk.Label(help_window, text=help_text,font=("Arial", 22), bg="#ADD8E6")
    help_label.pack(pady=5)

    # Set anchor to center for vertical alignment
    help_label.pack(pady=(10, 20), padx=10, anchor="center")

    help_window.mainloop()
# Create the main Tkinter window
# root = tk.Tk()
# root.title("Skills Development - Help")

# Call the function to open the help page immediately upon running the script
open_help_page()

# Run the Tkinter event loop
# root.mainloop()
