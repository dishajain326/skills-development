import tkinter as tk
import subprocess
from PIL import ImageTk, Image

def open_login_page():
    login_window = tk.Toplevel(root)
    login_window.title("Login")
    # Add widgets for login page here

def open_signup_page():
    signup_window = tk.Toplevel(root)
    signup_window.title("Signup")
    # Add widgets for signup page here

def open_home_page():
    home_window = tk.Toplevel(root)
    home_window.title("Home")
    # Add widgets for home page here

def open_about_page():
    about_window = tk.Toplevel(root)
    about_window.title("About")
    # Add widgets for about page here

def open_profile_page():
    profile_window = tk.Toplevel(root)
    profile_window.title("Profile")
    
    # Add widgets for profile page here

def open_find_page():
    # Implement functionality for find page here
    find_window=tk.Toplevel(root)
    find_window.title("Find")

def open_help_page():
    # Implement functionality for help page here
    pass

def profile():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/profile1.py"])
def about():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/about.py"])
def help():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/help.py"])
def find():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/find.py"])
def home():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/home.py"])
def login():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/loginfinal.py"])
def signup():
    subprocess.run(["python", "C:/Users/pavan/OneDrive/Wise/signupfinal.py"])


# Create the main Tkinter window
root = tk.Tk()
root.title("Skills Development Project")

# Load and resize the background image
bg_image = Image.open("bg1.jpg")
bg_image = bg_image.resize((root.winfo_screenwidth(), root.winfo_screenheight()), Image.ANTIALIAS)
background = ImageTk.PhotoImage(bg_image)

# Create a canvas to hold the background image
canvas = tk.Canvas(root, width=root.winfo_screenwidth(), height=root.winfo_screenheight())
canvas.pack(fill="both", expand=True)
#root.configure(bg="gray")

# Add the background image to the canvas
canvas.create_image(0, 0, anchor="nw", image=background)

# Load and resize the logo image
logo_image = Image.open("logo4.jpg")
logo_image = logo_image.resize((180, 180), Image.ANTIALIAS)
logo = ImageTk.PhotoImage(logo_image)

# Create a logo label
logo_label = tk.Label(root, image=logo, bg="white")
logo_label.place(x=50, y=150)

# Create home, about, profile, find, and help buttons
home_button = tk.Button(root, bg="white", fg="black", text="Home", font=("Arial",17),command=home,borderwidth=3, highlightbackground="black")
home_button.place(x=10, y=20,width=100, height=42)
about_button = tk.Button(root, bg="white", fg="black",text="About", font=("Arial",17),command=about,borderwidth=3, highlightbackground="black")
about_button.place(x=120, y=20, width=100, height=42)
profile_button = tk.Button(root,bg="white", fg="black", text="Profile", font=("Arial",17),command=profile,borderwidth=3, highlightbackground="black")
profile_button.place(x=230, y=20, width=100, height=42)
find_button = tk.Button(root, bg="white", fg="black",text="Find", font=("Arial",17),command=find,borderwidth=3, highlightbackground="black")
find_button.place(x=340, y=20, width=100, height=42)
help_button = tk.Button(root, bg="white", fg="black",text="Help", font=("Arial",17),command=help,borderwidth=3, highlightbackground="black")
help_button.place(x=450, y=20, width=100, height=42)

# Create login and signup buttons on the right side
login_button = tk.Button(root, bg="white", fg="black",text="Login",font=("Arial",17), command=login,borderwidth=3, highlightbackground="black")
login_button.place(relx=0.9, y=20, width=100, height=42, anchor="ne")
signup_button = tk.Button(root,bg="white", fg="black", text="Signup",font=("Arial",17), command=signup,borderwidth=3, highlightbackground="black")
signup_button.place(relx=0.8, y=20, width=100, height=42, anchor="ne")

# Run the Tkinter event loop
root.mainloop()
