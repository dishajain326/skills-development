import tkinter as tk

def open_find_page():
    # Create a new window for the find page
    find_window = tk.Tk()
    find_window.title("Find - Skills Development")
    find_window.configure(bg="#ADD8E6")

    # Add content to the find page
    label = tk.Label(find_window, text="Find - Skills Development", font=("Arial", 20, "bold"), bg="#ADD8E6")
    label.pack(pady=(10, 5), fill='x')

    find_text = """
    Welcome to the Skills Development project - Find Page!

    This page allows you to search for various resources related to engineering skills development.
    You can search for topics, tutorials, videos, and more.

    Get started by entering your search query in the search bar above.
    """
    find_label = tk.Label(find_window, font=("Arial", 22), text=find_text, bg="#ADD8E6")
    find_label.pack(pady=(5, 10), padx=10)

    find_window.mainloop()
# Create the main Tkinter window
# root = tk.Tk()
# root.title("Skills Development - Find")

# Call the function to open the find page immediately upon running the script
open_find_page()

# Run the Tkinter event loop
# root.mainloop()
