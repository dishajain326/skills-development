import tkinter as tk

def open_home_page():
    # Create a new window for the home page
    home_window = tk.Tk()
    home_window.title("Home - Skills Development")

    # Set background color
    home_window.configure(bg="#ADD8E6")

    # Add content to the home page
    label = tk.Label(home_window, text="Welcome to Skills Development!", font=("Arial",24 , "bold"), bg="#ADD8E6")
    label.pack(pady=10)

    services_text = """
    Services:
    
            1.Skills Enhancement\n  
      2.Tutorial Videos\n
     3.Related Texts\n
           4.Progress Tracking\n
           5.Career Guidances\n
    """
    services_label = tk.Label(home_window, text=services_text, font=("Arial", 20), bg="#ADD8E6")
    services_label.pack(pady=5)

    about_text = """
    About:
    
    Skills Development is a project aimed at providing resources and support for engineering students to enhance their skills.
    This application provides various resources such as workshops, tutorials, videos, and exercises to help students develop their skills and excel in their careers.
    """
    about_label = tk.Label(home_window, text=about_text, font=("Arial", 16), bg="#ADD8E6")
    about_label.pack(pady=5)

    # Run the Tkinter event loop for the home page window
    home_window.mainloop()

# Call the function to open the home page immediately upon running the script
open_home_page()
