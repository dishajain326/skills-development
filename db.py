# db.py
import sqlite3
import bcrypt

conn = sqlite3.connect('logindetails.db')
cursor = conn.cursor()

def create_table():
    cursor.execute('''
                   CREATE TABLE IF NOT EXISTS users(
                   username TEXT NOT NULL,
                   password TEXT NOT NULL)''')
    conn.commit()

def signup(username, password):
    cursor.execute('SELECT username FROM users WHERE username=?', [username])
    if cursor.fetchone() is not None:
        return False  # Username already exists
    else:
        # encoded_password = password.encode('utf-8')
        # hashed_password = bcrypt.hashpw(encoded_password, bcrypt.gensalt())
        cursor.execute('INSERT INTO users VALUES (?, ?)', [username,password])
        conn.commit()
        return True  # Signup successful

def login(username, password):
    cursor.execute('SELECT username, password FROM users WHERE username=?', [username])
    user_data = cursor.fetchone()
    if user_data is not None:
        stored_password = user_data[1]
        if(password == stored_password):
            return True  # Login successful
    return False  # Username or password is invalid